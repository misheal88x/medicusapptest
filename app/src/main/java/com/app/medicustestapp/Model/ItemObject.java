package com.app.medicustestapp.Model;

import com.google.gson.annotations.SerializedName;

public class ItemObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("date") private String date = "";
    @SerializedName("info") private String info = "";
    @SerializedName("color") private String color = "";
    @SerializedName("value") private String value = "";
    @SerializedName("symbol") private String symbol = "";
    @SerializedName("insight") private String insight = "";
    @SerializedName("category") private String category = "";

    public ItemObject(int id,
                      String date,
                      String info,
                      String color,
                      String value,
                      String symbol,
                      String insight,
                      String category) {
        this.id = id;
        this.date = date;
        this.info = info;
        this.color = color;
        this.value = value;
        this.symbol = symbol;
        this.insight = insight;
        this.category = category;
    }

    public ItemObject() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getInsight() {
        return insight;
    }

    public void setInsight(String insight) {
        this.insight = insight;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
