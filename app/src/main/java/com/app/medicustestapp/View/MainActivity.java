package com.app.medicustestapp.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.medicustestapp.Adapters.ItemsAdapter;
import com.app.medicustestapp.Base.BaseActivity;
import com.app.medicustestapp.Base.BaseFunctions;
import com.app.medicustestapp.Model.ItemObject;
import com.app.medicustestapp.Network.BaseResponse;
import com.app.medicustestapp.R;
import com.app.medicustestapp.ViewModel.ItemsViewModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    ItemsViewModel viewModel;

    private TextView title;
    private RelativeLayout loader_layout,no_internet_layout,no_data_layout,error_layout;
    private RecyclerView recycler;
    private ItemObject[] list;
    private LinearLayoutManager layoutManager;
    private ItemsAdapter adapter;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_main);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        initRecycler();
        selectView(0);
        title.setVisibility(View.GONE);
        viewModel = ViewModelProviders.of(this).get(ItemsViewModel.class);
        viewModel.getData().observe(this, new Observer<BaseResponse>() {
            @Override
            public void onChanged(BaseResponse baseResponse) {
                if (baseResponse == null){
                    //Error
                    selectView(3);
                    return;
                }
                //Success
                if (baseResponse.getError() == 0){
                    hideAll();
                    String j = new Gson().toJson(baseResponse.getObject());
                    ItemObject[] success = new Gson().fromJson(j,ItemObject[].class);
                    if (success.length > 0){
                        title.setVisibility(View.VISIBLE);
                        adapter.setItemsList(success);
                        BaseFunctions.runAnimation(recycler,0,adapter);
                    }else {
                        selectView(2);
                    }

                }
                //Error in service
                else if (baseResponse.getError() == 1){
                    selectView(3);
                }
                //No internet connection
                else if (baseResponse.getError() == 2){
                    selectView(1);
                }
            }
        });
    }

    @Override
    public void init_views() {
        title = findViewById(R.id.title);
        loader_layout = findViewById(R.id.loading_layout);
        error_layout = findViewById(R.id.error_layout);
        no_data_layout = findViewById(R.id.no_data_layout);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        recycler = findViewById(R.id.recycler);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void set_fragment_place() {

    }

    void initRecycler(){
        list = new ItemObject[0];
        adapter = new ItemsAdapter(this,list);
        layoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
    }

    void selectView(int index){
        RelativeLayout[] ls = new RelativeLayout[]{
                loader_layout,
                no_internet_layout,
                no_data_layout,
                error_layout
        };
        for (int i = 0; i < 4; i++) {
            if (i == index){
                ls[i].setVisibility(View.VISIBLE);
            }else {
                ls[i].setVisibility(View.GONE);
            }
        }
    }

    void hideAll(){
        loader_layout.setVisibility(View.GONE);
        no_data_layout.setVisibility(View.GONE);
        no_internet_layout.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
    }
}
