package com.app.medicustestapp.View;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.transition.Fade;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.medicustestapp.Base.BaseActivity;
import com.app.medicustestapp.Dialogs.InfoDialog;
import com.app.medicustestapp.Model.ItemObject;
import com.app.medicustestapp.R;
import com.google.gson.Gson;
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class DetailsActivity extends BaseActivity {


    private ItemObject item = null;
    Fade fade;
    View decor;

    private TextView category, symbol_text, symbol, date, result_title, info, insight;
    private ImageView info_icon;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_details);
    }


    @Override
    public void init_activity(Bundle savedInstanceState) {
        fade = new Fade();
        decor = getWindow().getDecorView();
        fade.excludeTarget(decor.findViewById(R.id.action_bar_container), true);
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        getWindow().setEnterTransition(fade);
        getWindow().setExitTransition(fade);

        item = new Gson().fromJson(getIntent().getStringExtra("item"), ItemObject.class);

        category.setText(item.getCategory());
        symbol_text.setText(item.getSymbol());
        symbol.setText(item.getSymbol());
        date.setText(item.getDate());
        result_title.setText("Your result is " + item.getValue());
        info.setText(item.getInfo());
        insight.setText(item.getInsight());

        if (item.getColor().equals("#E64A19")) {
            symbol.setBackground(getResources().getDrawable(R.drawable.red_circle));
            symbol.setTextColor(getResources().getColor(R.color.colorPrimary));
            result_title.setTextColor(getResources().getColor(R.color.colorPrimary));
        } else {
            symbol.setBackground(getResources().getDrawable(R.drawable.blue_circle));
            symbol.setTextColor(getResources().getColor(R.color.blue));
            result_title.setTextColor(getResources().getColor(R.color.blue));
        }
    }

    @Override
    public void init_views() {
        category = findViewById(R.id.category);
        symbol_text = findViewById(R.id.symbol_text);
        symbol = findViewById(R.id.symbol);
        date = findViewById(R.id.date);
        result_title = findViewById(R.id.result_title);
        info = findViewById(R.id.info);
        insight = findViewById(R.id.insight);
        info_icon = findViewById(R.id.info_icon);
    }

    @Override
    public void init_events() {
        info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InfoDialog dialog = new InfoDialog(DetailsActivity.this,item.getInfo());
                dialog.show();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }
}
