package com.app.medicustestapp.Repository;

import android.app.Application;

import com.app.medicustestapp.Model.ItemObject;
import com.app.medicustestapp.Network.BaseResponse;
import com.app.medicustestapp.Network.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ItemsRepository {



    public LiveData<BaseResponse> callApi() {
        final MutableLiveData<BaseResponse> apiResponse = new MutableLiveData<>();
        RetrofitClient.callAPI().getItems().enqueue(new Callback<List<ItemObject>>() {
            @Override
            public void onResponse(Call<List<ItemObject>> call, Response<List<ItemObject>> response) {
                if (response.isSuccessful()) {
                    apiResponse.postValue(new BaseResponse(response.body()));
                } else {
                    apiResponse.postValue(new BaseResponse(1));
                }
            }

            @Override
            public void onFailure(Call<List<ItemObject>> call, Throwable t) {
                apiResponse.postValue(new BaseResponse(2));
            }
        });
        return apiResponse;
    }
}
