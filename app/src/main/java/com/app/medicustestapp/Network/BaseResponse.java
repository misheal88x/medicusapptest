package com.app.medicustestapp.Network;

import com.app.medicustestapp.Model.ItemObject;

import java.util.ArrayList;

public class BaseResponse {
    private Object object;
    private int errorCode;

    public BaseResponse(Object object) {
        this.object = object;
        this.errorCode = 0;
    }

    public BaseResponse(int error) {
        this.errorCode = error;
        this.object = null;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public int getError() {
        return errorCode;
    }

    public void setError(int error) {
        this.errorCode = error;
    }

    public static BaseResponse ERROR_STATE = new BaseResponse(0);
    public static BaseResponse SUCCESS_STATE = new BaseResponse(new ArrayList<ItemObject>());
}
