package com.app.medicustestapp.Network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import androidx.lifecycle.MediatorLiveData;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    public static final String BASE_URL = "https://retoolapi.dev/hZZ5j8/";

    private static Retrofit retrofit;

    public static Retrofit getRetrofitClient(){
        if (retrofit == null){
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)

                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static APIs callAPI(){
        APIs a = getRetrofitClient().create(APIs.class);
        return a;
    }

}
