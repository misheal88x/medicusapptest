package com.app.medicustestapp.Network;

import com.app.medicustestapp.Model.ItemObject;

import java.util.List;

import androidx.lifecycle.LiveData;
import io.reactivex.Flowable;
import retrofit2.Call;
import retrofit2.http.GET;

public interface APIs {
    @GET("biomarkers")
    Call<List<ItemObject>> getItems();
}
