package com.app.medicustestapp.Base;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.app.medicustestapp.R;
import com.app.medicustestapp.Utils.LocaleHelper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {


    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private App app;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.setLocale(this, "en");
        set_layout();
        init_views();
        set_fragment_place();
        init_events();
        init_activity(savedInstanceState);
        changeStatusBarColor();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public abstract void set_layout();

    public abstract void init_activity(Bundle savedInstanceState);

    public abstract void init_views();

    public abstract void init_events();

    public abstract void set_fragment_place();

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        BaseFunctions.runBackSlideAnimation(this);
    }

    public void changeStatusBarColor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.white));
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }
}
