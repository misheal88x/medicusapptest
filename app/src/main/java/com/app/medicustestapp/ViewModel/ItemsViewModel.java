package com.app.medicustestapp.ViewModel;

import com.app.medicustestapp.Network.BaseResponse;
import com.app.medicustestapp.Repository.ItemsRepository;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

public class ItemsViewModel extends ViewModel {
    public MediatorLiveData<BaseResponse> mApiResponse;
    private ItemsRepository mApiRepo;

    public ItemsViewModel() {
        mApiResponse = new MediatorLiveData<>();
        mApiRepo = new ItemsRepository();
    }

    public LiveData<BaseResponse> getData() {
        mApiResponse.addSource(mApiRepo.callApi(), new Observer<BaseResponse>() {
            @Override
            public void onChanged(@Nullable BaseResponse apiResponse){
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }
}
