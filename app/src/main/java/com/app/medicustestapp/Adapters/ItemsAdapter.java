package com.app.medicustestapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.medicustestapp.Model.ItemObject;
import com.app.medicustestapp.R;
import com.app.medicustestapp.View.DetailsActivity;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.MyViewHolder> {
    private Activity context;
    private ItemObject[] list;
    private Fade fade;
    private View decor;

    public ItemsAdapter(Activity context, ItemObject[] list) {
        this.context = context;
        this.list = list;

        fade = new Fade();
        decor = context.getWindow().getDecorView();
        fade.excludeTarget(decor.findViewById(R.id.action_bar_container), true);
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        context.getWindow().setEnterTransition(fade);
        context.getWindow().setExitTransition(fade);
    }

    public void setItemsList(ItemObject[] moviesList) {
        this.list = moviesList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final ItemObject o = list[position];
        if (o.getColor().equals("#E64A19")) {
            holder.symbol.setBackground(context.getResources().getDrawable(R.drawable.red_circle));
            holder.symbol.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.symbol.setBackground(context.getResources().getDrawable(R.drawable.blue_circle));
            holder.symbol.setTextColor(context.getResources().getColor(R.color.blue));
        }
        if (o.getSymbol() == null || o.getSymbol().equals("")) {
            holder.layout.setVisibility(View.GONE);
            return;
        }
        holder.symbol.setText(o.getSymbol());
        holder.date.setText(o.getDate());
        holder.value.setText(o.getValue());

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("item", new Gson().toJson(o));
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        context, holder.symbol, ViewCompat.getTransitionName(holder.symbol));
                context.startActivity(intent,options.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.length;
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private TextView symbol, date, value;
        private RelativeLayout layout;

        public MyViewHolder(View itemView) {
            super(itemView);

            symbol = itemView.findViewById(R.id.symbol);
            date = itemView.findViewById(R.id.date);
            value = itemView.findViewById(R.id.value);
            layout = itemView.findViewById(R.id.layout);

        }
    }
}
