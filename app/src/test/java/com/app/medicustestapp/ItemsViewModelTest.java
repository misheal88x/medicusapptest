package com.app.medicustestapp;

import com.app.medicustestapp.Model.ItemObject;
import com.app.medicustestapp.Network.BaseResponse;
import com.app.medicustestapp.Repository.ItemsRepository;
import com.app.medicustestapp.ViewModel.ItemsViewModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.matchers.StacktracePrintingMatcher;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(JUnit4.class)
public class ItemsViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Mock
    public ItemsRepository repository;
    private ItemsViewModel viewModel;
    @Mock
    Observer<BaseResponse> observer;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        repository = mock(ItemsRepository.class);
        viewModel = new ItemsViewModel();
        viewModel.getData().observeForever(observer);
    }

    @Test
    public void testNull(){
        when(repository.callApi()).thenReturn(null);
        assertNotNull(viewModel.getData());
        assertTrue(viewModel.getData().hasObservers());
    }

    @Test
    public void testFetchSuccess() throws InterruptedException {
        MutableLiveData<BaseResponse> empty = new MutableLiveData<>();
        MutableLiveData<BaseResponse> full = new MutableLiveData<>();
        BaseResponse baseResponse1 = new BaseResponse(new ArrayList<ItemObject>());
        BaseResponse baseResponse2 = new BaseResponse(new ArrayList<ItemObject>());
        empty.postValue(baseResponse1);
        full.postValue(baseResponse2);
        when(repository.callApi()).thenReturn(empty);
        viewModel.getData();
        CountDownLatch latch = new CountDownLatch(1);
        latch.await(5, TimeUnit.SECONDS);
        assertEquals(baseResponse2.getError(),viewModel.mApiResponse.getValue().getError());
    }

    @After
    public void tearDown() throws Exception {
        repository = null;
        viewModel = null;
    }
}
